# git

git是一个分布式版本控制系统

##### 基本概念
版本库.git文件：当我们使用git管理文件时，一般它会多出一个git文件，我们将此称之为版本库

工作区：本地项目存放文件的位置

暂存区：暂时存放文件的地方，是通过add命令将工作区的文件添加到缓冲区

本地仓库：通常情况下，我们使用commit命令可以将暂存区的文件添加到本地仓库

远程仓库：例如当我们使用GitHub托管我们的项目时，它就是一个远程仓库

##### 配置命令
`git config --list`  列出当前配置
`git config --local --list`  列出Repository配置
`git config --global --list`  列出全局配置
`git config --system --list`  列出系统配置
配置用户信息
`git config --global user.name "your name"`  配置用户名
`git config --global user.email "youremail@github.com"`  配置用户邮箱

#### Git常用指令
`git init`  初始化本地库
`git status`  查看本地库状态
`git add`  添加到暂存区
`git add -A ` 全部提交
`git commit -m`  '日志信息' 提交到本地库
`git log`  查看详细版本信息
`git reflog`  查看版本信息
`git remote -v`  查看远程仓库是否连接
`git clone url`  克隆 自动关联远程仓库
`git remote add origin url`  手动关联远程仓库
`git push origin 分支名`  推送远程仓库
`git pull` 拉取远程仓库
`git checkout -b 本地分支名 origin/远程分支名`  切换并创建本地分支映射远程分支
`git stach`  将修改的内容保存至堆栈区
`git stach pop` 将当前stash中的内容弹出，应用到当前分支对应的工作目录上
`git rebase`  在另一个分支基础之上重新应用，用于把一个分支的修改合并到当前分支，代码历史更加的清晰
`git reset`  重置，用来将当前 branch 重置到另外一个 commit
`git revert`  撤销某次操作，此次操作之前和之后的commit和history都会保留，并且把这次撤销
作为一次最新的提交

#### git分支管理
`git branch`  查看本地分支
`git branch -r`  查看远程分支
`git branch -a`  查看本地和远程分支
`git checkout <branch-name>`  从当前分支切换到其他分支
`git checkout -b <branch-name>`  创建并切换到新建分支
`git checkout -d <branch-name>`  删除分支
`git merge <branch-name>`  当前分支与指定分支合并
`git branch --merged`  查看哪些分支已经合并到当前分支
`git branch --no-merged`  查看哪些分支没有合并到当前分支
`git branch -v`  查看各个分支最后一个提交对象的信息
`git push origin --d <branch-name>`  删除远程分支 
`git branch -m <oldbrnach-name>` `<newbrnach-name>`  重命名分支

