# husky
第二封装git hooks,用来在git不同阶段执行不同任务的
强制校验提交信息
1、下包：
````shell
yarn add husky
````

2、执行生成`.husky`文件夹
````shell
husky install
````
如不通过全局安装，通过scripts执行指令

3、添加钩子（hook）动作
````shell
npx husky add .husky/commit-msg "npx --no-install commitlint --edit"
````

下载依赖
````shell
commitlint  // 语法
@commitlint/cli   // cli指令
@commitlint/config-conventional  // 规则
````
添加commitlint.config.js,配置校验规则

4、执行校验

# 项目基础建设

###  团队约定
#### eslint
1、下包lint-staged
2、在`scripts`字段添加`lint`运行`lint-staged`指令
3、监听文件变化
4、添加git hooks pre-commit git add
````shell
npx husky add .husky/pre-commit "npm run lint"
````

a:husky生成钩子  npx husky add .husky/钩子名称 钩子执行的指令
b:commitlint配置message提交信息
c:lint-staged 检测文件变化执行eslint修复
#### 开发基础建设
##### 配置别名
减少引用的麻烦,方便后期的修改
webpack.confog.js中
````shell
resolve:{
  alias:{
    '名称':"路径"
  }
}
````
#####  api拆分
方便统一管理接口地址
接口方法复用性

虚拟dom：描述dom节点的js对象
使用虚拟dom:减少dom开销,真实的dom携带属性过多，直接操作dom会引起页面重绘和回流，react性能比较高，diff算法，比较虚拟dom区别，获取差异，更新视图，可以做到只把变化的部分重新渲染,提高渲染效率的过程，跨平台开发，虚拟 DOM 可以进行更方便地跨平台操作 js对象可以运行在不同平台 但是dom只能在浏览器


##### jsx是React.createElement语法糖

##### ajax和fetch会受同源策略限制，需要解决跨域问题，开发环境通过webpack devServer proxy(反向代理,服务器去请求数据) 解决跨域问题

# 目录

````shell
|src
    |--components
             |--List
                   |--index.js
                   |--style.module.scss
    |api
       |--module
               |--list.js
       |index.js
    |utils
          |--httptool
                    |--index.js
    |--App.js
````



# axios二次封装
ajax是发http请求，用于浏览器和服务器通信，属于一种技术
http请求只能浏览器主动向服务器发起请求
1、创建ajax对象
2、连接服务器 open
3、send发送数据
4、接收响应值


````shell
import axios from 'axios';


//config  基础配置
//failcallback
export default (config, failCk) => {

    const httpUtils = axios.create({
        timeout: 10000, //超时
        baseURL: ''   //根地址
    });

    //添加拦截器

    //接收响应值时执行  （请求的then之前）
    httpUtils.interceptors.response.use((response) => {
        return Promise.resolve(response.data)
    }, (error) => {
        //弱网提示
        if (error.code === 'ECONNABORTED') {
            failCk('网络超时请刷新重试')
            //如不返回会执行then
            return Promise.reject(error);
        }
    });
    return httpUtils;
}; 
````























